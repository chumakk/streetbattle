import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';

export function showWinnerModal(fighter) {
  const title = `Сongratulations, ${fighter.name}`;
  const bodyElement = createBodyElement(fighter);
  const onClose = () => {
    location.reload();
  };
  showModal({ title, bodyElement, onClose });
}

function createBodyElement(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const { source, name, health } = fighter;
  const image = createElement({
    tagName: 'img',
    className: 'modal-image',
    attributes: {
      src: source,
      title: name,
      alt: name,
    },
  });
  const congratulations = createElement({
    tagName: 'div',
    className: 'modal-congratulations',
  });
  congratulations.innerHTML = `Victory, victory,sweet victory... We knew that <span>${name}</span> would win this fight, at least we believed in it !!! <span>${name}</span> will be able to defeat anyone else (health left <span>${health}</span>).`;

  const wishes = createElement({
    tagName: 'div',
    className: 'modal-wishes',
  });
  wishes.innerText = `...Shh, fight one more time`;

  bodyElement.append(image, congratulations, wishes);
  return bodyElement;
}
