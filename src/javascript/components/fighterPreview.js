import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (!fighter) {
    return fighterElement;
  }

  const fighterName = createElement({
    tagName: 'div',
    className: 'fighter-preview___name',
  });
  fighterName.innerHTML = fighter.name;

  const fighterImage = createFighterImage(fighter);

  const fighterAbilities = createFighterAbilities(fighter);

  fighterElement.append(fighterName, fighterImage, fighterAbilities);

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterAbilities(fighter) {
  const { health, attack, defense } = fighter;
  
  const fighterAbilities = createElement({
    tagName: 'div',
    className: 'fighter-preview___abilities',
  });

  const fighterHealth = createElement({
    tagName: 'div',
    className: 'fighter-preview___health',
  });
  fighterHealth.innerHTML = `<span>Health</span> ${health}`;

  const fighterAttack = createElement({
    tagName: 'div',
    className: 'fighter-preview___attack',
  });
  fighterAttack.innerHTML = `<span>Attack</span> ${attack}`;

  const fighterDefense = createElement({
    tagName: 'div',
    className: 'fighter-preview___defense',
  });
  fighterDefense.innerHTML = `<span>Defense</span> ${defense}`;

  fighterAbilities.append(fighterHealth, fighterAttack, fighterDefense);

  return fighterAbilities;
}