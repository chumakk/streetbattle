import { controls } from '../../constants/controls';

function onKeyDown(e, firstFighter, secondFighter, healthFighters, isPressedKeys, resolve) {
  // set pressed key and add state 'block' to fighter if needed
  switch (e.code) {
    case controls.PlayerOneAttack:
      isPressedKeys.PlayerOneAttack = 1;
      break;
    case controls.PlayerOneBlock:
      isPressedKeys.PlayerOneBlock = 1;
      document.querySelector('.arena___fighter.arena___left-fighter').classList.add('block');
      break;
    case controls.PlayerTwoAttack:
      isPressedKeys.PlayerTwoAttack = 1;
      break;
    case controls.PlayerTwoBlock:
      isPressedKeys.PlayerTwoBlock = 1;
      document.querySelector('.arena___fighter.arena___right-fighter').classList.add('block');
      break;
    case controls.PlayerOneCriticalHitCombination[0]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[0]] = 1;
      break;
    case controls.PlayerOneCriticalHitCombination[1]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[1]] = 1;
      break;
    case controls.PlayerOneCriticalHitCombination[2]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[2]] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[0]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[0]] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[1]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[1]] = 1;
      break;
    case controls.PlayerTwoCriticalHitCombination[2]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[2]] = 1;
      break;
  }

  // make one hit
  makeHit(isPressedKeys, firstFighter, secondFighter, healthFighters);

  // checking for a winner
  if (firstFighter.health == 0 || secondFighter.health == 0) {
    resolve(firstFighter.health == 0 ? secondFighter : firstFighter);
  }
}

function makeHit(isPressedKeys, firstFighter, secondFighter, healthFighters) {
  const [healthFirstFighter, healthSecondFighter] = healthFighters;

  const isPlayerOneCriticalHitCombination =
    isPressedKeys.PlayerOneCriticalHitCombination.KeyQ &&
    isPressedKeys.PlayerOneCriticalHitCombination.KeyW &&
    isPressedKeys.PlayerOneCriticalHitCombination.KeyE;
  const isPlayerTwoCriticalHitCombination =
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyU &&
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyI &&
    isPressedKeys.PlayerTwoCriticalHitCombination.KeyO;

  // Can firstFighter make the hit
  if (
    ((isPressedKeys.PlayerOneAttack && !isPressedKeys.PlayerTwoBlock) ||
      (isPlayerOneCriticalHitCombination && firstFighter.readyCriticalHit)) &&
    !isPressedKeys.PlayerOneBlock
  ) {
    let damage;
    if (isPlayerOneCriticalHitCombination && firstFighter.readyCriticalHit) {
      firstFighter.readyCriticalHit = false;
      firstFighter.isCritical = true;
      damage = getDamage(firstFighter, secondFighter);
      setTimeout(() => (firstFighter.readyCriticalHit = true), 10000);
    } else {
      damage = getDamage(firstFighter, secondFighter);
    }

    animateHit('.arena___fighter.arena___left-fighter', '.arena___fighter.arena___right-fighter');

    secondFighter.health -= damage;
    secondFighter.health = secondFighter.health < 0 ? 0 : secondFighter.health;

    const rightHealthIndicator = document.querySelector('#right-fighter-indicator');
    rightHealthIndicator.style.width = `${(secondFighter.health / healthSecondFighter) * 100}%`;
  }

  // Can secondFighter make the hit
  if (
    ((isPressedKeys.PlayerTwoAttack && !isPressedKeys.PlayerOneBlock) ||
      (isPlayerTwoCriticalHitCombination && secondFighter.readyCriticalHit)) &&
    !isPressedKeys.PlayerTwoBlock
  ) {
    let damage;

    if (isPlayerTwoCriticalHitCombination && secondFighter.readyCriticalHit) {
      secondFighter.readyCriticalHit = false;
      secondFighter.isCritical = true;
      damage = getDamage(secondFighter, firstFighter);
      setTimeout(() => (secondFighter.readyCriticalHit = true), 10000);
    } else {
      damage = getDamage(secondFighter, firstFighter);
    }

    animateHit('.arena___fighter.arena___right-fighter', '.arena___fighter.arena___left-fighter');

    firstFighter.health -= damage;
    firstFighter.health = firstFighter.health < 0 ? 0 : firstFighter.health;

    const leftHealthIndicator = document.querySelector('#left-fighter-indicator');
    leftHealthIndicator.style.width = `${(firstFighter.health / healthFirstFighter) * 100}%`;
  }
}

function animateHit(attackerSelector, defenderSelector) {
  const attacker = document.querySelector(attackerSelector);
  const defender = document.querySelector(defenderSelector);
  attacker.classList.add('hitting');
  defender.classList.add('defending');
  setTimeout(() => {
    attacker.classList.remove('hitting');
    defender.classList.remove('defending');
  }, 200);
}

function onKeyUp(e, isPressedKeys) {
  // delete set key and delete state 'block' to fighter if needed
  switch (e.code) {
    case controls.PlayerOneAttack:
      isPressedKeys.PlayerOneAttack = 0;
      break;
    case controls.PlayerOneBlock:
      isPressedKeys.PlayerOneBlock = 0;
      document.querySelector('.arena___fighter.arena___left-fighter').classList.remove('block');
      break;
    case controls.PlayerTwoAttack:
      isPressedKeys.PlayerTwoAttack = 0;
      break;
    case controls.PlayerTwoBlock:
      isPressedKeys.PlayerTwoBlock = 0;
      document.querySelector('.arena___fighter.arena___right-fighter').classList.remove('block');
      break;
    case controls.PlayerOneCriticalHitCombination[0]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[0]] = 0;
      break;
    case controls.PlayerOneCriticalHitCombination[1]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[1]] = 0;
      break;
    case controls.PlayerOneCriticalHitCombination[2]:
      isPressedKeys.PlayerOneCriticalHitCombination[controls.PlayerOneCriticalHitCombination[2]] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[0]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[0]] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[1]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[1]] = 0;
      break;
    case controls.PlayerTwoCriticalHitCombination[2]:
      isPressedKeys.PlayerTwoCriticalHitCombination[controls.PlayerTwoCriticalHitCombination[2]] = 0;
      break;
  }
}

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    const isPressedKeys = {
      PlayerOneAttack: 0,
      PlayerOneBlock: 0,
      PlayerTwoAttack: 0,
      PlayerTwoBlock: 0,
      PlayerOneCriticalHitCombination: { KeyQ: 0, KeyW: 0, KeyE: 0 },
      PlayerTwoCriticalHitCombination: { KeyU: 0, KeyI: 0, KeyO: 0 },
    };

    const healthFighters = [firstFighter.health, secondFighter.health];

    // set ability for criticalHit
    firstFighter.readyCriticalHit = true;
    secondFighter.readyCriticalHit = true;

    window.addEventListener('keydown', (e) =>
      onKeyDown(e, firstFighter, secondFighter, healthFighters, isPressedKeys, resolve)
    );
    window.addEventListener('keyup', (e) => onKeyUp(e, isPressedKeys));
  });
}

export function getDamage(attacker, defender) {
  if (attacker.isCritical) {
    return getHitPower(attacker);
  }
  const damage = getHitPower(attacker) - getBlockPower(defender);

  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  if (fighter.isCritical) {
    fighter.isCritical = false;
    return fighter.attack * 2;
  }
  const criticalHitChance = Math.random() + 1;

  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;

  return fighter.defense * dodgeChance;
}
